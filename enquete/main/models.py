from django.db import models
from django.utils import timezone
import datetime

# Create your models here.

class Autor(models.Model):
    nome = models.CharField(max_length = 200)
    cidade = models.CharField(max_length = 200)
    email = models.CharField(max_length = 200)
    class Meta:
        verbose_name_plural = 'autores'
    def __str__(self):
        return self.nome

class Pergunta(models.Model):
    texto = models.CharField(max_length = 200)
    data_publicacao = models.DateTimeField('Data de Publicação')
    autor = models.ForeignKey(Autor, on_delete=models.CASCADE, blank=True, null=True, default=None)
    def __str__(self):
        return self.texto
    def publicada_recentemente(self):
        agora = timezone.now()
        return agora-datetime.timedelta(days=1) <= self.data_publicacao <= agora
    publicada_recentemente.admin_order_field = 'data_publicacao'
    publicada_recentemente.boolean = True
    publicada_recentemente.short_description = 'recentemente publicada?'

class Opcao(models.Model):
    texto = models.CharField(max_length = 200)
    votos = models.IntegerField(default = 0)
    pergunta = models.ForeignKey(Pergunta, on_delete=models.CASCADE)
    def __str__(self):
        return self.texto
    class Meta:
        verbose_name_plural = 'opções'

