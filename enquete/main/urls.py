from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
    path('enquete',views.EnqueteView.as_view(), name='enquete'),
    path('enquete/<int:pk>', views.DetalhesView.as_view(), name='detalhes'),
    path('enquete/<int:pk>/resultado', views.ResultadoView.as_view(), name='resultado'),
    path('enquete/<int:id_pergunta>/votar', views.votacao, name='votacao'),
    path('autor/<int:id_autor>', views.autor, name='autor'),
]