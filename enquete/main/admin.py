from django.contrib import admin
from .models import Pergunta, Opcao, Autor
# Register your models here.

class OpcaoInline(admin.TabularInline):
    model = Opcao
    extra = 2

class PerguntaAdmin(admin.ModelAdmin):
    fields =[
        #(None, {'fields': ['texto']}),
        #('Informações de data', {'fields':['data_publicacao']}),
        'data_publicacao', 'texto', 'autor'
    ]
    inlines = [OpcaoInline]
    list_display =('id','texto', 'data_publicacao', 'publicada_recentemente')
    list_filter =['data_publicacao']
    search_fields = ['texto']


admin.site.register(Pergunta, PerguntaAdmin)

admin.site.register(Autor)