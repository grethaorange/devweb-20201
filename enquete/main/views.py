from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import Pergunta, Opcao, Autor
from django.urls import reverse
from django.views import generic
from django.utils import timezone
# Create your views here.

def index(request):
    return render(request, 'main/index2.html', {})

class EnqueteView(generic.ListView):
    template_name = 'main/index.html'
    context_object_name = 'ultimas_perguntas'
    def get_queryset(self):
        return Pergunta.objects.filter(
            data_publicacao__lte=timezone.now()
            ).order_by('-data_publicacao')[:5]

class DetalhesView(generic.DetailView):
    model = Pergunta
    template_name = 'main/detalhes.html'
    def get_queryset(self):
        return Pergunta.objects.filter(data_publicacao__lte=timezone.now())

class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'main/resultado.html'

def autor(request, id_autor):
    autor = get_object_or_404(Autor, pk=id_autor)
    context = {
        'autor':autor,
    }
    return render(request, 'main/autor.html', context)

def votacao(request, id_pergunta):
    pergunta = get_object_or_404(Pergunta, pk=id_pergunta)
    try:
        opcao_selecionada = pergunta.opcao_set.get(pk=request.POST['opcao'])
    except (KeyError, Opcao.DoesNotExist):
        context = {
            'pergunta':pergunta,
            'error_message':'Selecione uma opção válida',
        }
        return render(request, 'main/detalhes.html', context)
    else:
        opcao_selecionada.votos += 1
        opcao_selecionada.save()
        return HttpResponseRedirect(reverse('main:resultado', args=(pergunta.id,)))