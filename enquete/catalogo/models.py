from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.

class Categoria(models.Model):
    nome = models.CharField(max_length=200)
    def __str__(self):
        return self.nome

class Texto(models.Model):
    titulo = models.CharField(max_length=200)
    texto = models.TextField()
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    publico = models.BooleanField(default=False)
    data_publicacao = models.DateTimeField(default=timezone.now())
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    imagem = models.ImageField(upload_to='imagens/textos', blank=True)

    def __str__(self):
        return self.titulo
