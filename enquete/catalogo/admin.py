from django.contrib import admin
from .models import Texto, Categoria
# Register your models here.

admin.site.register(Texto)
admin.site.register(Categoria)
