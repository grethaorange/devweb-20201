from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, redirect
from django.http import HttpResponse, Http404
from .models import Texto, Categoria
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout, authenticate
# Create your views here.

@login_required
def index(request):
    return render(request, 'catalogo/index.html', {})

def texto(request, autor , titulo):
    try:
        texto = Texto.objects.get(Q(autor__username=autor)&Q(titulo=titulo))
    except:
        raise Http404('Texto não encontrado')
    if((request.user == texto.autor) | texto.publico):
        context = {
            'texto':texto,
        }
        return render(request, 'catalogo/texto.html', context)
    raise Http404()

def autor(request, username):
    autor = get_object_or_404(User, username = username)
    if((request.user == autor)):
        textos = Texto.objects.filter(autor = autor).order_by('-data_publicacao')
    else:
        textos = Texto.objects.filter(Q(autor = autor)&Q(publico = True)).order_by('-data_publicacao')
    context = {
        'textos':textos,
    }
    return render(request, 'catalogo/autor.html', context)

def categoria(request, nome):
    categoria = get_object_or_404(Categoria, nome = nome)
    textos = Texto.objects.filter(Q(autor = request.user)&Q(categoria = categoria)).order_by('-data_publicacao')
    context = {
        'textos':textos,
        'categoria':categoria,
    }
    return render(request, 'catalogo/categoria.html', context)

@login_required
def publicar(request):
    titulo = request.POST['titulo']
    texto = request.POST['texto']
    categoria_id = request.POST['categoria']
    categoria = get_object_or_404(Categoria, id=categoria_id)
    publico = False
    if 'publico' in request.POST:
        publico = True
    try:
        texto = Texto.objects.get(Q(autor=request.user)&Q(titulo=titulo))
    except:
        texto = Texto(
            titulo = titulo,
            texto = texto,
            autor = request.user,
            categoria = categoria,
            publico = publico
        )
        if 'imagem' in request.FILES:
            imagem = request.FILES['imagem']
            texto.imagem = imagem
        texto.save()
        return HttpResponseRedirect(reverse('catalogo:texto', kwargs={'autor': request.user.username, 'titulo':titulo}))
    else:
        raise Http404("Já existe um texto com esse título. Tente com outro diferente.")

def alterar_texto(request):
    titulo = request.POST['titulo']
    texto_id = request.POST['texto-id']
    texto_txt = request.POST['texto']
    categoria_id = request.POST['categoria']
    categoria = get_object_or_404(Categoria, id=categoria_id)
    publico = False
    if 'publico' in request.POST:
        publico = True
    try:
        texto = Texto.objects.get(Q(autor=request.user)&Q(titulo=titulo)&~Q(id=texto_id))
    except:
        texto = get_object_or_404(Texto, id=texto_id)
        texto.titulo = titulo
        texto.texto = texto_txt
        texto.categoria = categoria
        texto.publico = publico
        texto.save()
        return HttpResponseRedirect(reverse('catalogo:texto', kwargs={'autor': request.user.username, 'titulo':titulo}))
    else:
        raise Http404('Já existe outro texto com o mesmo título no seu perfil')

@login_required
def escrita(request):
    categorias = Categoria.objects.all()
    context = {
        'categorias':categorias,
    }
    return render(request, 'catalogo/escrita.html', context)

@login_required
def deletar(request):
    texto_id = request.POST['texto-id']
    texto = get_object_or_404(Texto, id=texto_id)
    if texto.autor == request.user:
        if texto.imagem:
            texto.imagem.delete()
        texto.delete()
        return redirect('autor/'+request.user.username)
    else:
        raise Http404()

def categorias(request):
    categorias = Categoria.objects.all()
    context = {
        'categorias':categorias,
    }
    return render(request, 'catalogo/categorias.html', context)

'''
def editar(request):
    texto_id = request.POST['texto-id']
    texto = get_object_or_404(Texto, id=texto_id)
    if texto.autor == request.user:
        texto.editar()
        return redirect('autor/'+request.user.username)
    else:
        raise Http404()
'''

def editar(request, texto_id):
    texto = get_object_or_404(Texto, id=texto_id)
    categorias = Categoria.objects.all()
    context = {
        'categorias':categorias,
        'texto':texto,
    }
    return render(request, 'catalogo/escrita.html', context)


def cadastro(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('catalogo:index'))
    return render(request, 'registration/cadastro.html')

def criar_conta(request):
    username = request.POST['username']
    nome = request.POST['nome']
    sobrenome = request.POST['sobrenome']
    senha = request.POST['senha']
    try:
        user = User.objects.get(username=username)
    except:
        user = User(
            username = username,
            first_name = nome,
            last_name = sobrenome,
        )
        user.set_password(senha)
        user.save()
        return HttpResponseRedirect(reverse('catalogo:autor', kwargs={'username': request.user.username}))
    else:
        context ={
            'nome':nome,
            'sobrenome':sobrenome,
            'username':username,
            'username_ja_utilizado':True,
        }
        return render(request, 'registration/cadastro.html', context)

@login_required
def editar_perfil(request):
    return render(request, 'catalogo/editar_perfil.html')

@login_required
def alterar_perfil(request):
    nome = request.POST['nome']
    sobrenome = request.POST['sobrenome']
    username = request.POST['username']
    try:
        user = User.objects.get(Q(username=username)&~Q(id=request.user.id))
    except:
        user = request.user
        user.first_name = nome,
        user.last_name = sobrenome,
        user.username = username,
        user.save()
        mensagem_de_sucesso = "Perfil alterado com êxito!!"
        context = {
            'mensagem_de_sucesso':mensagem_de_sucesso,
        }
        return render(request, 'catalogo/alterar_perfil.html', context)
    else:
        mensagem_de_erro = "Nome já existe. Por favor, tente outro."
        context = {
            'mensagem_de_erro':mensagem_de_erro,
        }
        return render(request, 'catalogo/alterar_perfil.html', context)

@login_required
def alterar_senha(request):
    senha_atual = request.POST['senha-atual']
    nova_senha = request.POST['nova-senha']
    if request.user.check_password(senha_atual):
        request.user.set_password(nova_senha)
        request.user.save()
        authenticate(username = request.user.username, password=nova_senha)
        login(request, request.user)
        mensagem_de_sucesso = "Senha alterado com êxito!!"
        context ={
            'mensagem_de_sucesso':mensagem_de_sucesso,
        }
        return render(request, 'catalogo/alterar_perfil.html', context)
    else:
        mensagem_de_erro = "A senha está incorreta. Por favor, tente novamente."
        context = {
            'mensagem_de_erro':mensagem_de_erro,
        }
        return render(request, 'catalogo/alterar_perfil.html', context)


@login_required
def alterar_imagem_perfil(request):
    if 'imagem' in request.FILES:
        imagem = request.FILES['imagem']
        if request.user.autor.imagem_perfil:
            request.user.autor.imagem_perfil.delete()
        request.user.autor.imagem_perfil = imagem
        request.user.autor.save()
    return HttpResponseRedirect(reverse('catalogo:autor', kwargs={'username': request.user.username}))


@login_required
def remover_imagem_perfil(request):
    if request.user.imagem_perfil:
        request.user.autor.imagem_perfil.delete()
    return HttpResponseRedirect(reverse('catalogo:autor', kwargs={'username': request.user.username}))
