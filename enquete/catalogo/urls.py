from django.urls import path
from . import views

app_name = 'catalogo'

urlpatterns = [
    path('', views.index, name='index'),
    path('autor/<str:autor>/texto/<str:titulo>', views.texto, name='texto'),
    path('autor/<str:username>', views.autor, name='autor'),
    path('categoria/<str:nome>', views.categoria, name='categoria'),
    path('escrita', views.escrita, name='escrita'),
    path('publicar', views.publicar, name='publicar'),
    path('deletar', views.deletar, name='deletar'),
    path('categorias', views.categorias, name='categorias'),
    path('editar/<int:texto_id>', views.editar, name='editar'),
    path('alterar_texto', views.alterar_texto, name='alterar_texto'),
    path('cadastro', views.cadastro, name='cadastro'),
    path('criar_conta', views.criar_conta, name='criar_conta'),
    path('editar_perfil', views.editar_perfil, name='editar_perfil'),
    path('alterar_perfil', views.alterar_perfil, name='alterar_perfil'),
    path('alterar_senha', views.alterar_senha, name='alterar_senha'),
    path('alterar_imagem_perfil', views.alterar_imagem_perfil, name='alterar_imagem_perfil'),
    path('remover_imagem_perfil', views.remover_imagem_perfil, name='remover_imagem_perfil'),
]